package com.example.springpractice.services

import com.example.springpractice.CreateProductDTO
import com.example.springpractice.ProductRepository
import com.example.springpractice.ReadProductDTO
import com.example.springpractice.UpdateProductDTO
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

// Product에 대한 비즈니스 로직을 처리한다.
@Component
class ProductService {
    @Autowired
    lateinit var productRepository: ProductRepository

    // Read 요청을 처리
    fun getProducts(): List<ReadProductDTO> {
        val product = productRepository.findAll()
        return product.map { it.toReadProductDTO() }
    }

    // Create 요청을 처리
    @Transactional
    fun createProduct(createProductDTO: CreateProductDTO): CreateProductDTO {
        val product = productRepository.save(createProductDTO.toEntity())
        return product.toCreateProductDTO();
    }

    fun getProduct(id: Long): ReadProductDTO {
        return productRepository.findById(id)
            .orElseThrow { NotFoundException("ID가 ${id}인 Product를 찾을 수 없습니다.") }
            .toReadProductDTO()
    }

    fun deleteProduct(id: Long) {
        productRepository.delete(
            productRepository.findById(id)
                .orElseThrow { NotFoundException("ID가 ${id}인 Product를 찾을 수 없습니다.") })
    }

    fun updateProduct(id: Long, properties: Map<String, Any>) {
        val productToUpdate = productRepository.findById(id)
            .orElseThrow { NotFoundException("ID가 ${id}인 Product를 찾을 수 없습니다.") }

        val modelProperties = productToUpdate::class.memberProperties;
        UpdateProductDTO::class.memberProperties.forEach {
            val dtoPropName = it.name;
            if(properties.contains(dtoPropName)) {
                val value = properties[dtoPropName] as String;

//                 productToUpdate에 변경값 적용
                modelProperties.find { it.name == dtoPropName }
                    .takeIf { it is KMutableProperty<*> }
                    ?.let { it as KMutableProperty<*> }
                    ?.let { it.setter.call(productToUpdate, value)}
            }
        }
        productRepository.save(productToUpdate)
    }
}