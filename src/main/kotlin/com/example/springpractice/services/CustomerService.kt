package com.example.springpractice.services

import com.example.springpractice.models.Customer
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class CustomerService: ICustomerService {
    companion object {
        val initialCustomers = arrayOf(
            Customer(1,"Kotlin"),
            Customer(2,"Lotlin"),
            Customer(3,"Jotlin")
        )
    }

    val customers = ConcurrentHashMap<Int, Customer>(initialCustomers.associateBy { it.id })

    override fun getCustomer(id: Int) = customers[id]

    override fun createCustomer(customer: Customer) {
        customers[customer.id] = customer
    }

    override fun deleteCustomer(id: Int) {
        customers.remove(id)
    }

    override fun updateCustomer(id: Int, customer: Customer) {
        customer.id = id;
        customers[id] = customer
    }

    override fun searchCustomers(name: String): List<Customer>
        = customers.filter { it.value.name.contains(name, true) }
        .map { it.value }
}