package com.example.springpractice.contollers

import com.example.springpractice.CreateProductDTO
import com.example.springpractice.ReadProductDTO
import com.example.springpractice.services.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

// Product를 외부 요청에 대해 처리하는 인터페이스들을 정의한다.
@RestController
class ProductController {
    @Autowired
    private lateinit var productService: ProductService

    // GET으로 Product를 여럿 조회
    @GetMapping("/products", produces = ["application/json"])
    fun getProducts(): ResponseEntity<Any> {
        return ResponseEntity
            .ok()
            .body(productService.getProducts())
    }

    // GET으로 Product 하나를 조회
    @GetMapping("/product/{id}", produces = ["application/json"])
    fun getProduct(
        @PathVariable(required = true) id: Long
    ): ResponseEntity<ReadProductDTO?>
            = ResponseEntity
        .ok()
        .body(productService.getProduct(id))

    // POST로 Product를 생성
    @PostMapping("/product")
    fun createProduct(
        @RequestBody createProductDTO: CreateProductDTO
    ): ResponseEntity<Any> {
        productService.createProduct(createProductDTO)
        return ResponseEntity
            .ok()
            .body(true)
    }

    // DELETE로 Product 삭제
    @DeleteMapping("/product/{id}")
    fun deleteProduct(
        @PathVariable id: Long
    ): ResponseEntity<Any> {
        productService.deleteProduct(id)
        return ResponseEntity
            .ok()
            .body(true)
    }

    // PUT으로 Product 수정
    @PatchMapping("/product/{id}")
    fun patchProduct(
        @PathVariable id: Long,
        @RequestBody updateProperties: Map<String, Any>
    ): ResponseEntity<Any> {
        productService.updateProduct(id, updateProperties)
        return ResponseEntity
            .ok()
            .body(true)
    }
}