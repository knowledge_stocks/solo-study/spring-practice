package com.example.springpractice.contollers

import com.example.springpractice.models.Customer
import com.example.springpractice.services.ICustomerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    private lateinit var customerService: ICustomerService

    // GET으로 Customer를 여럿 조회
    // json 형식으로 응답하기 위해서는 produces를 ["application/json"]로 설정한다.
    @GetMapping("/customers", produces = ["application/json"])
    fun getCustomers(
        @RequestParam(required = false, defaultValue = "") name: String
    ): ResponseEntity<List<Customer?>>
        = ResponseEntity
        .ok()
        .body(customerService.searchCustomers(name))

    // GET으로 Customer 하나를 조회
    @GetMapping("/customer/{id}", produces = ["application/json"])
    fun getCustomer(
        @PathVariable(required = true) id: Int // required는 true가 기본값이라고 한다.
    ): ResponseEntity<Customer?>
        = ResponseEntity
        .ok()
        .body(customerService.getCustomer(id))

    // POST로 Customer를 생성
    @PostMapping("/customer")
    fun createCustomer(
        @RequestBody(required = true) customer: Customer
    ): ResponseEntity<Any> {
        customerService.createCustomer(customer)
        return ResponseEntity
            .ok()
            .body(true)
    }

    // PUT으로 Customer를 수정
    @PutMapping("/customer/{id}")
    fun updateCustomer(
        @PathVariable id: Int,
        @RequestBody customer: Customer
    ): ResponseEntity<Any> {
        if(customerService.getCustomer(id) == null) {
            throw Exception("customer id: ${id} cannot find")
        }
        customerService.updateCustomer(id, customer)
        return ResponseEntity
            .ok()
            .body(true)
    }

    // DELETE로 Customer를 삭제
    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(
        @PathVariable id: Int
    ): ResponseEntity<Any> {
        if(customerService.getCustomer(id) == null) {
            throw Exception("customer id: ${id} cannot find")
        }
        customerService.deleteCustomer(id)
        return ResponseEntity
            .ok()
            .build()
    }
}