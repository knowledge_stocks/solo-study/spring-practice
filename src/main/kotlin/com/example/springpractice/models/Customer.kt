package com.example.springpractice.models

data class Customer(var id: Int = 0, var name: String);