package com.example.springpractice.models

import com.example.springpractice.CreateProductDTO
import com.example.springpractice.ReadProductDTO
import java.time.OffsetDateTime
import javax.persistence.*

enum class Category {
    Phone, Laptop, Keyboard
}

// API에서 사용할 Product 모델
// 모델과 DTO의 차이가 뭔지 아직 잘은 모르지만
// 대충 모델은 디비의 스키마에 직접적인 영향을 주는 것 같다.
// DTO는 API에서 실질적으로 사용하는 속성들만 가지는 객체인 것 같다.
// 예를들어 createDatetime와 updateDateTime는 DB에 기록할 필요는 있지만, API에서 굳이 사용할 필요 없는 데이터이기 때문에 DTO에서 빠진게 아닌가 싶다.
@Entity
data class Product (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null, // 기본키 만드는 구문인 듯 하다.
    var name: String, // 제품의 이름과 카테고리는 바뀌면 안 되므로 val이 맞겠지만 업데이트 API 연습을 위해 var로 변경
    @Enumerated(EnumType.STRING) var category: Category, // Enum을 문자열 형식으로 저장하도록한다. 지정하지 않으면 숫자형으로 들어간다.
    val createDatetime: OffsetDateTime = OffsetDateTime.now(),
    var updateDateTime: OffsetDateTime? = null
) {
    // Entity에는 반드시 인자가 없는 생성자가 있어야 한다.
    // 안 그러면 jpa에서 'no default constructor for entity'라는 예외가 발생한다.
    constructor(): this(null, "", Category.Phone)

    // Read 요청에 대해 필요한 속성들을 DTO로 리턴해주기 위한 함수
    fun toReadProductDTO(): ReadProductDTO {
        return ReadProductDTO(id = id, name = name, category = category)
    }

    // Create 요청에 대해 필요한 속성들을 DTO로 리턴해주기 위한 함수
    fun toCreateProductDTO(): CreateProductDTO {
        return CreateProductDTO(name = name, category = category)
    }
}